import React from "react";
import {songsMock} from "../../mock/songs.mock";
import {Song} from "../../model/song.model";

export default class SongsInfo extends React.Component<Song> {
    render() {
        return (
            <div>
                <h1 className="songName">{this.props.name}</h1>
                <h3 className="artistName">{this.props.artist}</h3>
                <h3 className="albumName">{this.props.album}</h3>
            </div>
        );
    }
}

