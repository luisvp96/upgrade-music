import React from "react";
import '../../Styles/style.css';
import './Login.css';
import logo from "../../img/logo_drawing.png";

import {Button, Form} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export class Login extends React.Component {
    render() {
        return (
            <div className="container-fluid loginContainer">
                <div className="loginContainer2">
                    <div className="loginHeader">
                        <div className="loginLogo">
                            <Link to="/">
                                <img src={logo} alt="logo" width="250px"/>
                            </Link>
                        </div>
                        <div className="titlesLogin">
                            <h2 className="whiteText title">Upgrade Music</h2>
                            <h4 className="subtitleLogin">Connect the world with power!</h4>
                        </div>
                    </div>
                    <div className="loginForm">
                        <Form>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label className="formLabelText">Email address</Form.Label>
                                <Form.Control className="formLabelText loginInputs" type="email" placeholder="Enter email" />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label className="formLabelText">Password</Form.Label>
                                <Form.Control className="formLabelText loginInputs" type="password" placeholder="Password" />
                            </Form.Group>
                            <div className="loginBtnDiv">
                                <Button type="submit" className="colorCotaBtn loginPageBtn">
                                    <Link to="/playlist">Login</Link>
                                </Button>
                            </div>
                        </Form>
                        <Link to="/">
                            <p className="formLabelText forgotPass">Forgot your password?</p>
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}