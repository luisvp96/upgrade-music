import {Nav, Button, Navbar, Form, FormControl} from 'react-bootstrap';
import React from "react";
import './Footer.css';
import {FaBackward, FaFastForward, FaPause, FaPlay, FaStop, FaUser, FaVolumeDown, FaVolumeMute, FaVolumeUp} from "react-icons/fa";
import everybody from "../../img/everybody.jpg";

export class Footer extends React.Component {
    render() {
        return (
            <div className="test">
                <footer>
                    <div className="FooterContainer">
                        <img src={everybody} alt="wpp" className="FooterAlbum"/>
                        <div className="songInfo">
                            <p>Everybody Loves The Sunshine</p>
                            <p>Roy Ayers Ubiquity</p>
                        </div>
                        <div className="playIcons">
                            <FaBackward size={32} className="playMusicIcons"/>
                            <FaPlay size={32} className="playMusicIcons"/>
                            <FaFastForward size={32} className="playMusicIcons"/>
                        </div>
                        <div className="volumeIcons">
                            <FaVolumeMute size={32} className="playMusicIcons"/>
                            <FaVolumeDown size={32} className="playMusicIcons"/>
                            <FaVolumeUp size={32} className="playMusicIcons"/>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}