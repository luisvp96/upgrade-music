import React from "react";
import '../../Styles/style.css';
import './ListOfPlaylists.css';
import {NavBar} from "../NavBar/NavBar";
import { FaPlus, FaShareAlt, FaSortAlphaDown } from "react-icons/fa";
import {Footer} from "../Footer/Footer";

export class ListOfPlaylists extends React.Component {
    render() {
        return (
            <div>
                <div className="container-fluid listOfPlaylists">
                    <NavBar/>
                    <div className="cardPlaylists">
                        <div className="topBarPlaylist">
                            <div className="iconsAlignmentLeft">
                                <FaSortAlphaDown size={32} className="playlistIcons"/>
                                <FaShareAlt size={32} className="playlistIcons"/>
                            </div>
                            <div className="iconsAlignmentRight">
                                <FaPlus size={32} className="playlistIcons"/>
                            </div>
                        </div>
                        <div className="contentPLaylist">
                            <div className="playlistDetails">
                                <p>Playlist 1</p>
                                <p>Number of Songs</p>
                                <p>Length</p>
                                <p>Genders</p>
                            </div>
                            <div className="playlistDetails">
                                <p>Playlist 2</p>
                                <p>Number of Songs</p>
                                <p>Length</p>
                                <p>Genders</p>
                            </div>
                            <div className="playlistDetails">
                                <p>Playlist 3</p>
                                <p>Number of Songs</p>
                                <p>Length</p>
                                <p>Genders</p>
                            </div>
                            <div className="playlistDetails">
                                <p>Playlist 4</p>
                                <p>Number of Songs</p>
                                <p>Length</p>
                                <p>Genders</p>
                            </div>
                        </div>
                    </div>

                </div>
                <Footer/>
            </div>
        )
    };
};