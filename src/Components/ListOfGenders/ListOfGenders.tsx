import React from "react";
import '../../Styles/style.css';
import './ListOfGenders.css';
import {NavBar} from "../NavBar/NavBar";
import { FaPlus, FaShareAlt, FaSortAlphaDown } from "react-icons/fa";
import {Footer} from "../Footer/Footer";
import rock from "../../img/rock.jpeg";
import reggae from "../../img/reggae.jpeg";
import reagaton from "../../img/reagaeton.jpeg";
import indie from "../../img/indie.jpeg";
import house from "../../img/house.jpeg";
import edm from "../../img/edm.jpeg";
import classical from "../../img/classical.jpeg";
import pop from "../../img/pop.jpeg";


export class ListOfGenders extends React.Component {
    render() {
        return (
            <div>
                <div className="container-fluid listOfGenders">
                    <NavBar/>
                    <div className="gendersCard">
                        <div className="gendersFirstRow">
                            <div className="gendersWrapper">
                                <img src={rock} alt="wpp" className="gendersPic"/>
                                <h3>Rock</h3>
                            </div>
                            <div className="gendersWrapper">
                                <img src={reggae} alt="wpp" className="gendersPic"/>
                                <h3>Reggae</h3>
                            </div>
                            <div className="gendersWrapper">
                                <img src={reagaton} alt="wpp" className="gendersPic"/>
                                <h3>Reggaeton</h3>
                            </div>
                            <div className="gendersWrapper">
                                <img src={pop} alt="wpp" className="gendersPic"/>
                                <h3>Pop</h3>
                            </div>
                        </div>
                        <div className="gendersSecondRow">
                            <div className="gendersWrapper">
                                <img src={indie} alt="wpp" className="gendersPic"/>
                                <h3>Indie</h3>
                            </div>
                            <div className="gendersWrapper">
                                <img src={house} alt="wpp" className="gendersPic"/>
                                <h3>House</h3>
                            </div>
                            <div className="gendersWrapper">
                                <img src={edm} alt="wpp" className="gendersPic"/>
                                <h3>Electronic Dance Music</h3>
                            </div>
                            <div className="gendersWrapper">
                                <img src={classical} alt="wpp" className="gendersPic"/>
                                <h3>Classical</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    };
};