import React from "react";
import '../../Styles/style.css';
import './ListOfArtistsPage.css';

import {NavBar} from "../NavBar/NavBar";
import {Button, Form} from 'react-bootstrap';
import {FaUser} from "react-icons/fa";
import {Footer} from "../Footer/Footer";
import {songsMock} from "../../mock/songs.mock";
import SongsInfo from "../SongsInfo/SongsInfo";
import  ListOfArtists from "../ListOfArtists/ListOfArtists";

export class ListOfArtistsPage extends React.Component {

    state = {
        musics: songsMock,
        currentMusic: 'Riptide'
    };

    render() {
        return (
            <div>
                <div>
                    <NavBar/>
                </div>
                <div className="container-fluid listOfArtistsContainer">
                    <div className="listOfArtistsContainer2">


                        {this.state.musics.filter((elem,index) => (
                            this.state.musics
                        )).map((elem, index) => (
                            <ListOfArtists
                                key={index}
                                name={elem.name}
                                artist={elem.artist}
                                album={elem.album}
                                duration={elem.duration}
                            />
                        ))}



                    </div>
                </div>
                <div className="FooterFlex">
                    <Footer/>
                </div>
            </div>
        );
    }
}