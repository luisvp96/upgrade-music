import React from "react";
import '../../Styles/style.css';
import './SignUp.css';
import logo from "../../img/logo_drawing.png";

import {Button, Form} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export class SignUp extends React.Component {
    render() {
        return (
            <div className="container-fluid signUpContainer">
                <div className="signUpHeader">
                    <h1 className="whiteText signUptitle">Create an account</h1>
                    <Link to="/">
                        <img src={logo} alt="logo" width="200"/>
                    </Link>
                </div>
                <div className="signUpForm">
                    <Form>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label className="formLabelText" placeholder="Name"
                                        aria-describedby="basic-addon1">Name
                            </Form.Label>
                            <Form.Control className="formLabelText signUpInputs" type="text" placeholder="Enter name" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label className="formLabelText" placeholder="Username"
                                        aria-describedby="basic-addon1">Username
                            </Form.Label>
                            <Form.Control className="formLabelText signUpInputs" type="text" placeholder="Enter username" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label className="formLabelText">Email address</Form.Label>
                            <Form.Control className="formLabelText signUpInputs" type="email" placeholder="Enter email" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label className="formLabelText">Password</Form.Label>
                            <Form.Control className="formLabelText signUpInputs" type="password" placeholder="Password" />
                        </Form.Group>
                        <div className="signUpBtnDiv">
                            <Button type="submit" className="colorCotaBtn signUpPageBtn">
                                <Link to="/playlist">Sign Up</Link>
                            </Button>
                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}