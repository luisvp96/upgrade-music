import React from "react";
import '../../Styles/style.css';
import './MainPage.css';
import {Button} from 'react-bootstrap';

import logo from "../../img/logo_drawing.png";
import {Link} from 'react-router-dom';

export class MainPage extends React.Component {
    render() {
        return (
            <div className="initialContainer">
                <div className="container flexClass">
                    <img src={logo} alt="logo" width="300px"/>
                    <h2 className="whiteText title">Upgrade Music</h2>
                    <h4 className="subtitle">Connect the world with power!</h4>
                    <div className="buttons">
                        <Button className="colorCotaBtn loginBtn">
                            <Link to="/login">Login</Link>
                        </Button>
                        <Button className="colorCotaBtn signBtn">
                            <Link to="/signUp">Sign Up</Link>
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}