import React from "react";
import {songsMock} from "../../mock/songs.mock";
import {Song} from "../../model/song.model";
import '../ListOfArtistsPage/ListOfArtistsPage.css';
import {FaUser} from "react-icons/fa";

export default class ListOfArtists extends React.Component<Song> {
    render() {
        return (
            <div className="listOfArtistsImgDiv">
                <div className="listOfArtistsAvatarDiv">
                    <div className="listOfArtistsAvatar">
                        <FaUser size={50} className="listOfArtistsUser"/>
                    </div>
                </div>
                <div className="listOfArtistsTextDiv">
                    <h3 className="listOfArtistsText">{this.props.artist}</h3>
                    <h4 className="listOfArtistsText">{this.props.album}</h4>
                </div>
            </div>

        );
    }
}
