import React from "react";
import {songsMock} from "../../mock/songs.mock";
import {Song} from "../../model/song.model";
import '../PlayMusicPage/PlayMusicPage.css';
import {FaPlay} from "react-icons/fa";

export default class SongsListPlayMusic extends React.Component<Song> {
    render() {
        return (
            <div className="songsListPlay">
                <div className="playSongListDiv">
                    <div className="playSongList">
                        <FaPlay size={20} className="playSongListIcon"/>
                    </div>
                </div>
                <div className="songNameArtistDiv">
                    <p className="songNamePlayList">{this.props.name}</p>
                    <p className="artistNamePlayList">{this.props.artist}</p>
                </div>
                <div className="songDurationDiv">
                    <div className="songDuration">{this.props.duration}</div>
                </div>
            </div>
        );
    }
}