import React from "react";
import '../../Styles/style.css';
import './PlaylistPage.css';
import {NavBar} from "../NavBar/NavBar";
import wpp from '../../img/playlistwpp.jpeg';
import {Footer} from "../Footer/Footer";
import {Link} from 'react-router-dom';




export class Playlist extends React.Component {

    componentDidMount() {
        this.state = {kindOfMusic: 'Rock'};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event:any) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event:any) {
        alert('Your gender is: ' + this.state);
        event.preventDefault();
    }

    render() {
        return (
            <div>
                <div className="bodyContainer">
                    <NavBar/>
                    <div className="creator">
                        <img src={wpp} alt="wpp" width="200px" className="coverAlbum"/>
                        <div className="playlistHelper">
                            <h3>Create your playlist here</h3>
                            <form onSubmit={this.handleSubmit} className="square">
                            <label htmlFor="Gender">
                                <select className="form-select" aria-label="Default select example">
                                    <option selected>Choose your gender</option>
                                    <option value="1">Rock</option>
                                    <option value="2">Pop</option>
                                    <option value="3">Indie</option>
                                    <option value="4">Reggae</option>
                                    <option value="5">Classical</option>
                                    <option value="6">Hip-Hop</option>
                                    <option value="7">Reggaeton</option>
                                    <option value="8">Heavy Metal</option>
                                    <option value="9">EDM</option>
                                </select>
                            </label>
                            </form>
                            <h3>Playlist plays for <label htmlFor="number">
                                    <select className="form-select" aria-label="Default select example">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </label> hours</h3>
                            <div className="CreateButton">
                                <div className="d-grid gap-2 d-md-block">
                                    <button type="submit" className="btn-create">
                                        <Link to="/playMusic" className="linkButton">Create</Link>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="FooterFlex">
                    <Footer/>
                </div>
            </div>
        );
    }
}