import {Nav, Button, Navbar, Form, FormControl} from 'react-bootstrap';
import React from "react";
import './NavBar.css';
import logo from "../../img/logo_drawing_tranparent.png";
import {FaUser} from "react-icons/fa";
import {Link} from 'react-router-dom';

export class NavBar extends React.Component {
    render() {
        return (
            <div>
                <Navbar className="navBarClass" expand="lg">
                    <Link to="/">
                        <img src={logo} alt="logo" width="50px"/>
                    </Link>

                    <Navbar.Toggle aria-controls="navbarScroll"/>
                    <Navbar.Collapse className="navBarFlex"  id="navbarScroll">
                        <Nav
                            className="mr-auto my-2 my-lg-0"
                            style={{maxHeight: '100px'}}
                            navbarScroll >
                            <Link to="/listofplaylists" className="listPlaylistTag">My Playlists</Link>
                            <Link to="/listofgenders" className="listGenderTag">My Genders</Link>
                            <Link to="/listofartists" className="listPlaylistTag">My Artists</Link>
                            <Nav.Link href="#action2">My Songs</Nav.Link>

                        </Nav>
                        <Form className="d-flex">
                            <FormControl
                                type="search"
                                placeholder="Search"
                                className="mr-2 searchInput"
                                aria-label="Search"
                            />
                            <Button className="navbarSearchBtn">Search</Button>
                        </Form>
                        <Link to="/myProfile">
                            <FaUser size={32} className="navbarUser"/>
                        </Link>

                    </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
}