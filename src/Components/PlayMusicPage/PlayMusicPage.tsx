import React from "react";
import '../../Styles/style.css';
import './PlayMusicPage.css';
import {NavBar} from "../NavBar/NavBar";
import {Footer} from "../Footer/Footer";
import {ProgressBar} from 'react-bootstrap';
import {
    FaVolumeMute, FaVolumeUp, FaVolumeDown, FaStop, FaPause, FaPlay, FaPlus,
    FaShareAlt, FaBackward, FaFastBackward, FaFastForward, FaForward, FaInfoCircle,
    FaStepForward, FaStepBackward, FaSortAlphaDown, FaSortAlphaDownAlt, FaHeart
} from "react-icons/fa";
import cover from '../../img/vance-joy-riptide-cover.jpg';

import {songsMock} from "../../mock/songs.mock";
import SongsInfo from "../SongsInfo/SongsInfo";
import  SongsListPlayMusic from "../SongsListPlayMusic/SongsListPlayMusic";

export class PlayMusic extends React.Component {

    state = {
        musics: songsMock,
        currentMusic: 'Riptide'
    };

    render() {
        return (
            <div>
                <div className="container-fluid playMusicContainer">
                    <NavBar/>
                    <div className="playMusicContainer2">
                        <div className="playMusicBody">
                            <div className=
                                     "songImage">
                                <img src={cover} alt="Cover of Vance Joy - Riptide" width="300px"/>
                            </div>
                            <div className="progressBarDiv">
                                <ProgressBar className="progressBar" now={60} />
                            </div>
                        </div>
                        <div className="songInfoPlayMusic">

                            {this.state.musics.filter((elem,index) => (
                                elem.name == this.state.currentMusic
                            )).map((elem, index) => (
                                <SongsInfo
                                    key={index}
                                    name={elem.name}
                                    artist={elem.artist}
                                    album={elem.album}
                                    duration={elem.duration}
                                />
                            ))}

                            <div className="playIconsGeralDiv">
                                <div className="playIconsPlayMusic">
                                    <FaPause size={32} className="playMusicIcons"/>
                                    <FaStop size={32} className="playMusicIcons"/>
                                    <FaBackward size={32} className="playMusicIcons"/>
                                    <FaPlay size={32} className="playMusicIcons"/>
                                    <FaFastForward size={32} className="playMusicIcons"/>
                                </div>
                                <div className="volumeIconsPlayMusic">
                                    <FaVolumeMute size={32} className="playMusicIcons"/>
                                    <FaVolumeDown size={32} className="playMusicIcons"/>
                                    <FaVolumeUp size={32} className="playMusicIcons"/>
                                </div>
                            </div>
                        </div>
                        <div className="songsListDiv">
                            <div className="songsList">
                                <div className="listIcons">
                                    <FaSortAlphaDown size={32} className="SongListIcons"/>
                                    <FaPlus size={32} className="SongListIcons"/>
                                    <FaHeart size={32} className="SongListIcons"/>
                                    <FaShareAlt size={32} className="SongListIcons"/>
                                </div>


                                <div className="songsListPlayGeral">
                                    <div className="songsListPlayScroll">

                                        {this.state.musics.filter((elem,index) => (
                                            this.state.musics
                                        )).map((elem, index) => (
                                            <SongsListPlayMusic
                                                key={index}
                                                name={elem.name}
                                                artist={elem.artist}
                                                album={elem.album}
                                                duration={elem.duration}
                                            />
                                        ))}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="FooterFlex">
                    <Footer/>
                </div>
            </div>
        );
    }
}