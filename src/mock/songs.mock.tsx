import {Song} from "../model/song.model";

export const songsMock = [
    new Song( 'Riptide', 'Vance Joy', 'Dream Your Life Away', '3:24'),
    new Song( 'The End', 'The Doors', 'The Doors', '11:40'),
    new Song( 'Old Man', 'Neil Young', 'Harvest', '3:24'),
    new Song( 'The Scientist', 'Coldplay', 'A Rush of Blood to the Head', '5:09'),
    new Song( 'Blue Monday', 'New Order', 'Singles', '7:30'),
    new Song( 'Easy On Me', 'Adele', '30', '3:24')
];

/* ADD COVER, GENDERS, ... */