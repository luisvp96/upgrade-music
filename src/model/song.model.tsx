export class Song {
        name: string;
        artist: string;
        album: string;
        duration: string;

    constructor(
        name: string,
        artist: string,
        album: string,
        duration: string
    )
    {
        this.name = name;
        this.artist = artist;
        this.album = album;
        this.duration = duration;
    }
}

/* ADD COVER, GENDERS, ... */