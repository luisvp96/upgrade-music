import React from "react";
import './Styles/style.css';
import './Components/MainPage/MainPage.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import {MainPage} from "./Components/MainPage/MainPage";
import {Login} from "./Components/Login/Login";
import {SignUp} from "./Components/SignUp/SignUp";
import {Playlist} from "./Components/PlaylistPage/PlaylistPage";
import {PlayMusic} from "./Components/PlayMusicPage/PlayMusicPage";
import {MyProfilePage} from "./Components/MyProfilePage/MyProfilePage";
import {ListOfPlaylists} from "./Components/ListOfPlayslists/ListOfPlaylists";
import {ListOfGenders} from "./Components/ListOfGenders/ListOfGenders";
import {ListOfArtistsPage} from "./Components/ListOfArtistsPage/ListOfArtistsPage";
/*import {Song} from "./model/song.model";*/
/* <Route path="/songModel" component={Song}></Route> */

export class App extends React.Component {
    render() {
        return (
            < >
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact component={MainPage}>
                        </Route>
                        <Route path="/playlist" component={Playlist}>
                        </Route>
                        <Route path="/playMusic" component={PlayMusic}>
                        </Route>
                        <Route path="/login" component={Login}>
                        </Route>
                        <Route path="/signUp" component={SignUp}>
                        </Route>
                        <Route path="/myProfile" component={MyProfilePage}>
                        </Route>
                        <Route path="/listofplaylists" component={ListOfPlaylists}>
                        </Route>
                        <Route path="/listofgenders" component={ListOfGenders}>
                        </Route>
                        <Route path="/listofartists" component={ListOfArtistsPage}>
                        </Route>
                    </Switch>
                </BrowserRouter>
            </>
        );
    }
}
